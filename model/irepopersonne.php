<?php
    require_once 'personne.php';
    interface IRepoPersonne {
        function ajouterPersonne(Personne $personne) : void;
        function modifierPersonne($id,Personne $personne) : void;
        function supprimerPersonne($id) : void;
        function rechercherPersonne($id) : Personne;
        /**
         * @return Personne[]
         */
        function getTousPersonne() : array;
    }

?>
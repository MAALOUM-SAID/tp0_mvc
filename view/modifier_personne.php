<?php 
  $title="Modifier une personne";
  require_once 'templates/head.php';
?>
<body>
    <div class="container">
<div class="form-body">
        <div class="row">
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Register Today</h3>
                        <form action="/personne/<?=$personne->get_id()?>" class="requires-validation" method="POST" novalidate>
                            <input type="hidden" name="_method" value="PUT">
                            <div class="col-md-12 mt-3">
                               <input class="form-control" type="text" name="id" value="<?=$personne->get_id()?>"  placeholder="ID" readonly required>
                            </div>
                            <div class="col-md-12 mt-3">
                               <input class="form-control" type="text" name="nom" value="<?=$personne->get_nom()?>" placeholder="nom" required>
                            </div>

                            <div class="col-md-12 mt-3">
                                <input class="form-control" type="email" name="prenom" value="<?=$personne->get_prenom()?>" placeholder="prenom" required>
                            </div>
                            <div class="col-md-12 mt-3">
                                <input class="form-control" type="email" name="age" value="<?=$personne->get_age()?>" placeholder="age" required>
                            </div>
                            <div class="form-button mt-3">
                                <button id="modifier" type="submit"  class="btn btn-primary">Modifier</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

</body>
</html>
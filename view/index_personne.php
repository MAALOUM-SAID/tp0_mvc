<?php 
  $title="Liste Des personne";
  require_once 'templates/head.php';
?>
<body>
  <div class="container mt-5">
<table class="table table-bordered">
<thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nom</th>
      <th scope="col">Prenom</th>
      <th scope="col">Age</th>
      <th scope="col" colspan=3 >Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($personnes as $personne): ?>
    <tr>
      <th scope="row"><?= $personne["id"] ?></th>
      <td><?= $personne["nom"] ?></td>
      <td><?= $personne["prenom"] ?></td>
      <td><?= $personne["age"] ?></td>
      <td>
        <form action="/personne/<?=$personne["id"]?>" method="POST">
        <input type="hidden" name="id" value="<?=$personne["id"]?>">
        <input type="hidden" name="_method" value="DELETE">
        <button type="submit" class="bg-white border-0">
          <i class="fa-solid fa-trash text-danger"></i></a></td>
        </button>
      </form>
      <td><a href="/personne/<?=$personne["id"]?>/edit"><i class="fa-solid fa-pen-to-square "></i></a></td>
      <td><a href=""><i class="fa-solid fa-circle-chevron-down text-success"></i></a></td>
    </tr>
    <?php endforeach; ?>

</table>
    <a href="/personne/create" class="btn btn-outline-success f-right">Ajouter</a>
    </div>
</body>
</html>
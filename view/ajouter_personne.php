<?php 
  $title="Ajouter une personne";
  require_once 'templates/head.php';
?>
<body>
    <div class="container">
<div class="form-body">
        <div class="row">
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Register Today</h3>
                        <form action="/personne" method="POST" class="requires-validation" novalidate>

                            <div class="col-md-12 mt-3">
                               <input class="form-control" type="text" name="id" placeholder="ID" required>
                            </div>
                            <div class="col-md-12 mt-3">
                               <input class="form-control" type="text" name="nom" placeholder="nom" required>
                            </div>

                            <div class="col-md-12 mt-3">
                                <input class="form-control" type="email" name="prenom" placeholder="prenom" required>
                            </div>
                            <div class="col-md-12 mt-3">
                                <input class="form-control" type="email" name="age" placeholder="age" required>
                            </div>
                            <div class="form-button mt-3">
                                <button id="ajouter" type="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>
</html>
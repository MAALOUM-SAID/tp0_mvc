<?php
    require_once "controlleur/personne_controller.php";
    require_once "model/repo_personne.php";
    $repoPersonne=new RepoPersonne();
    $controlleur=new PersonneController($repoPersonne);
    $modifier_request_true=false;
    $modifier_request_true2=false;
    $pattern="/personne\/\d+(\/edit)*/";
    $p2="/personne\/\d+/";
    if (preg_match($pattern,$_SERVER["REQUEST_URI"],$MATCHES)) {
        $modifier_request_true=true;
    }
    if ($_SERVER["REQUEST_METHOD"]=="POST") {
        if (preg_match($p2,$_SERVER["REQUEST_URI"],$MATCHES)) {
            $modifier_request_true2=true;
        }
    }
    switch ($_SERVER["REQUEST_METHOD"]) {
        case 'GET':
            if ($_SERVER["REQUEST_URI"]=="/personne") {
                
                $controlleur -> index();
            }
            else if($_SERVER["REQUEST_URI"]=="/personne/create"){
                $controlleur -> create();
                
            }
            else if($modifier_request_true){
                
                $controlleur -> edit();
                
            }
            break;
        case "POST":
            if ($_SERVER["REQUEST_URI"]=="/personne") {
                
                $controlleur -> store();
            }
            elseif ($_REQUEST["_method"]=="PUT" && $modifier_request_true2) {
                $controlleur -> update();
            }
            elseif ($_REQUEST["_method"]=='DELETE' && $modifier_request_true2) {
                $controlleur -> destroy();
            }
            break;
        
    }





?>
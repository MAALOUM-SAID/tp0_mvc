<?php
    require_once "model/Irepopersonne.php";
    class PersonneController {
        private IRepoPersonne $personne_repo;

        function __construct(IRepoPersonne $personne_repo){
            $this -> personne_repo = $personne_repo;
        }
        function index(){
            $personnes=$this -> personne_repo -> getTousPersonne();
            require_once "view/index_personne.php";
        }
        function create(){
            require_once "view/ajouter_personne.php";
        }
        function store(){
            $id=$_REQUEST['id'];
            $nom=$_REQUEST['nom'];
            $prenom=$_REQUEST['prenom'];
            $age=$_REQUEST['age'];
            $p = new Personne($id,$nom,$prenom,$age);
            $personnes=$this -> personne_repo -> ajouterPersonne($p);
            header('Location: /personne');
        }
        function edit(){
            $id=explode('/',$_SERVER["REQUEST_URI"])[2];
            $personne=$this -> personne_repo -> rechercherPersonne($id);
            require_once 'view/modifier_personne.php';

        }
        function update(){
            $id=$_REQUEST['id'];
            $nom=$_REQUEST['nom'];
            $prenom=$_REQUEST['prenom'];
            $age=$_REQUEST['age'];
            $p = new Personne($id,$nom,$prenom,$age);
            $personnes=$this -> personne_repo -> modifierPersonne($id,$p);
            header('Location: /personne');
        }
        function destroy(){
            $id=$_REQUEST['id'];
            $personnes=$this -> personne_repo -> supprimerPersonne($id);
            header('Location: /personne');
        }

    }

?>